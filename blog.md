# Message Queues 

## What is a Message Queue?
A queue is First In First Out datastructure. You can think like a car or any sequentialy processing data. A message queue is basically a queue which is placed in  between two points in connected system in order for them to communicate which each other. 


![alt](message_queue.png)

A message is a data transported between the sender and receiver. An example is one part of the system trigering an event in second part of the system. So to process this information the queue performs in asynchronously. ie, it doesn't lock the system by performing in synchronous manner ie, performing only after it have processed the message. This way of working leads to system crash. As there will many data in queue to process. Think this message queue as restaurant it have to process many orders and the order should be in priority. So if the restaurant supplier waits for every order to finish to take more order the restaurant will be reviewed very inefficient. Thats why we have to process the message queue async. 


- Message Queue will perisist the data information
- Easy to maintain and Scale 
- Used in microservices for long running tasks

## Popular tools available

There are many tools available for message queue service, some of the popular ones are listed below.

- RabbitMQ
- Apache Kafka
- Amazon MQ
- IBM MQ

Among these RabbitMQ is the most famous message queue software. It is based on AMQP O-9-1 version(Advanced Message Queueing Protocol). The middle part marked in Square box is rabbitMQ. 

![alt](rabbitmq2.png)

It is based on exchange which manage the order before adding to queue to process. Exchange is connected to many queues by bindings and these bindings are referenced with the binding keys.  
Exchange can occur in many ways like Fanout exchange: Exchange will duplicate the message to every single queue that it knows about. Direct Exchange: Routing key is compared to the binding key if it's a match then the message will move to the queue. 

Following are it's advantages. 

- Highly Flexible
- Cloud Friendly
- Cross Language friendly


## Enterprise Message Bus:

In a System Oriented architecture systems are able to communicate with each other with XML format of messages. Thus it creates a mess. Consider there are 3 systems connected with SOA it connects with each other causing 9 connections. This can be eliminated by adding a bus. Each system only connects to this bus and the bus communicates with each Node connected on the network forming one way communication.  

References- 
- [IBM rabbitMQ](https://www.youtube.com/watch?v=7rkeORD4jSw&t=301s)
- [Hussien Nasser](https://www.youtube.com/watch?v=W4_aGb_MOls)
- [GKVS](https://www.youtube.com/watch?v=oUJbuFMyBDk)
